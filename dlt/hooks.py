# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "dlt"
app_title = "DLT"
app_publisher = "Tristar Enterprises"
app_description = "Custom document and report for Digital Link Trading LLC"
app_icon = "octicon octicon-file-directory"
app_color = "blue"
app_email = "info@tristar-enterprises.com"
app_license = "MIT"

# setup wizard
boot_session = "dlt.startup.boot.boot_session"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/dlt/css/dlt.css"
# app_include_js = "/assets/dlt/js/dlt.js"

# include js, css files in header of web template
# web_include_css = "/assets/dlt/css/dlt.css"
# web_include_js = "/assets/dlt/js/dlt.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "dlt.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "dlt.install.before_install"
# after_install = "dlt.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "dlt.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

doc_events = {
	"Sales Invoice": {
		"validate" : [
				"dlt.utils.selling.validate_trade_license_expiry",
				"dlt.utils.validate_cost_center",
				"dlt.utils.validate_warehouse"
			],
		"before_save": [
				"dlt.utils.selling.validate_net_selling_price"
			],

		"before_submit": [
				"dlt.utils.selling.validate_net_selling_price"
			],
		"on_submit" : "dlt.utils.selling.send_email_to_printer_with_attachment"
	},
	"Quotation": {
		"validate" : [
				"dlt.utils.selling.validate_net_selling_price",
				"dlt.utils.validate_warehouse"
			],
	},
	"Sales Order": {
		"validate" : [
				"dlt.utils.selling.validate_trade_license_expiry",
				"dlt.utils.validate_cost_center",
				"dlt.utils.validate_warehouse"
			],
		"before_save": [
				"dlt.utils.selling.validate_net_selling_price"
			],
		"before_submit": [
				"dlt.utils.selling.validate_net_selling_price"
			],


	},
	"Delivery Note": {
		"validate" : [
				"dlt.utils.selling.validate_trade_license_expiry",
				"dlt.utils.selling.validate_if_delivery_note_already_exists",
				"dlt.utils.validate_cost_center"
			
			],
		"before_save": [
				"dlt.utils.selling.validate_net_selling_price"
			],

		"before_submit": [
				"dlt.utils.selling.validate_net_selling_price"
			],
		"on_submit" : "dlt.utils.selling.send_email_to_printer_with_attachment",
	},
	"Purchase Order": {
		"validate" : [
				"dlt.utils.validate_cost_center",
				"dlt.utils.validate_warehouse"
			],
	},
	"Purchase Receipt": {
		"validate" : [
				"dlt.utils.validate_cost_center",
				"dlt.utils.validate_warehouse"
			],
	},
	"Purchase Invoice": {
		"validate" : [
				"dlt.utils.validate_cost_center"
			],
	},
	"Payment Entry": {
		"on_cancel" : [
				"dlt.utils.account.delink_payment_entry"
			],
	},

}

default_mail_footer = ""

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"dlt.tasks.all"
# 	],
# 	"daily": [
# 		"dlt.tasks.daily"
# 	],
# 	"hourly": [
# 		"dlt.tasks.hourly"
# 	],
# 	"weekly": [
# 		"dlt.tasks.weekly"
# 	]
# 	"monthly": [
# 		"dlt.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "dlt.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#override_whitelisted_methods = {
 #	"erpnext.accounts.doctype.journal_entry.journal_entry.get_payment_entry_against_invoice": #"dlt.utils.account.get_payment_entry_against_invoice"
#}

