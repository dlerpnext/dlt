# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

#from __future__ import unicode_literals
#from frappe.utils import flt, get_datetime, getdate, date_diff, cint, nowdate
#from frappe import _, msgprint, throw
#import frappe

from __future__ import unicode_literals
import frappe, json
from frappe import _, scrub, ValidationError, msgprint
from erpnext.accounts.utils import get_outstanding_invoices, get_account_currency, get_balance_on
from erpnext.accounts.party import get_party_account
from erpnext.accounts.doctype.journal_entry.journal_entry \
	import get_average_exchange_rate, get_default_bank_cash_account
from erpnext.setup.utils import get_exchange_rate

from frappe.utils import cstr, cint, get_fullname, getdate, flt, comma_or, nowdate


@frappe.whitelist()
def make_internal_transfer_entry(doctype, docname):
	doc = frappe.get_doc(doctype, docname)
	if doc:
		if cint(doc.transfer_to_main_cash) == 1:
			frappe.throw(_("Cash is already transferred to main cash."))
			
		company_default_cash_account = frappe.db.get_value("Company", doc.company, "default_cash_account")
		if not company_default_cash_account:
			frappe.throw(_("Default cash account is not setup under company \n Please setup default cash account."))

		if doc.paid_to in company_default_cash_account:
			frappe.throw(_("Internal transfer can not be done between same account."))

		pe = frappe.new_doc(doctype)
		pe.posting_date = getdate()
		pe.mode_of_payment = doc.mode_of_payment
		pe.payment_type = "Internal Transfer"
		pe.paid_from = doc.paid_to
		pe.paid_from_account_currency = doc.paid_to_account_currency
		pe.paid_to = company_default_cash_account
		pe.paid_to_account_currency = doc.paid_to_account_currency
		pe.paid_amount = doc.paid_amount
		pe.base_paid_amount = doc.base_paid_amount
		pe.received_amount = doc.paid_amount
		pe.base_received_amount = doc.base_paid_amount
		pe.insert()
		pe.submit()
		frappe.db.set(doc, 'internal_transfer_payment_entry', pe.name)
		frappe.db.set(doc, 'transfer_to_main_cash', 1)
		frappe.db.commit()
		return pe.name


def delink_payment_entry(doc, method):
	linked_payment_entry = frappe.db.get_value("Payment Entry", {"internal_transfer_payment_entry": doc.name})
	if linked_payment_entry:
		pe = frappe.get_doc("Payment Entry", linked_payment_entry)
		frappe.db.set(pe, 'internal_transfer_payment_entry', "")
		frappe.db.set(pe, 'transfer_to_main_cash', 0)
		frappe.db.commit()


@frappe.whitelist()
def get_payment_entry(dt, dn, party_amount=None, bank_account=None, bank_amount=None):
	doc = frappe.get_doc(dt, dn)

	if dt in ("Sales Order", "Purchase Order") and flt(doc.per_billed, 2) > 0:
		frappe.throw(_("Can only make payment against unbilled {0}").format(dt))

	if dt in ("Sales Invoice", "Sales Order"):
		party_type = "Customer"
	elif dt in ("Purchase Invoice", "Purchase Order"):
		party_type = "Supplier"
	elif dt in ("Expense Claim"):
		party_type = "Employee"
	elif dt in ("Fees"):
		party_type = "Student"

	# party account
	if dt == "Sales Invoice":
		party_account = doc.debit_to
	elif dt == "Purchase Invoice":
		party_account = doc.credit_to
	elif dt == "Fees":
		party_account = doc.receivable_account
	else:
		party_account = get_party_account(party_type, doc.get(party_type.lower()), doc.company)

	party_account_currency = doc.get("party_account_currency") or get_account_currency(party_account)

	# payment type
	if (dt == "Sales Order" or (dt in ("Sales Invoice", "Fees") and doc.outstanding_amount > 0)) \
		or (dt=="Purchase Invoice" and doc.outstanding_amount < 0):
			payment_type = "Receive"
	else:
		payment_type = "Pay"

	# amounts
	grand_total = outstanding_amount = 0
	if party_amount:
		grand_total = outstanding_amount = party_amount
	elif dt in ("Sales Invoice", "Purchase Invoice"):
		grand_total = doc.base_grand_total if party_account_currency == doc.company_currency else doc.grand_total
		outstanding_amount = doc.outstanding_amount
	elif dt in ("Expense Claim"):
		grand_total = doc.total_sanctioned_amount
		outstanding_amount = doc.total_sanctioned_amount - doc.total_amount_reimbursed
	elif dt == "Fees":
		grand_total = doc.grand_total
		outstanding_amount = doc.outstanding_amount
	else:
		total_field = "base_grand_total" if party_account_currency == doc.company_currency else "grand_total"
		grand_total = flt(doc.get(total_field))
		outstanding_amount = grand_total - flt(doc.advance_paid)

	paid_to = frappe.db.get_value("Cost Center", doc.cost_center, "cash_account")
	if not paid_to:
		paid_to = frappe.db.get_value("Company", doc.company, "default_cash_account")

	paid_to_account_details = frappe.db.get_value("Account", paid_to,
			["account_currency", "account_type"], as_dict=1)


	paid_amount = received_amount = 0

	if party_account_currency == paid_to_account_details.account_currency:
		paid_amount = received_amount = abs(outstanding_amount)
	elif payment_type == "Receive":
		paid_amount = abs(outstanding_amount)
		if bank_amount:
			received_amount = bank_amount
	else:
		received_amount = abs(outstanding_amount)
		if bank_amount:
			paid_amount = bank_amount

	pe = frappe.new_doc("Payment Entry")
	pe.payment_type = payment_type
	pe.company = doc.company
	pe.posting_date = nowdate()
	pe.mode_of_payment = "Cash"
	pe.party_type = party_type
	pe.party = doc.get(scrub(party_type))
	pe.paid_from = party_account if payment_type=="Receive" else paid_to
	pe.paid_to = party_account if payment_type=="Pay" else paid_to
	pe.paid_from_account_currency = party_account_currency \
		if payment_type=="Receive" else paid_to_account_details.account_currency
	pe.paid_to_account_currency = party_account_currency if payment_type=="Pay" else paid_to_account_details.account_currency
	pe.paid_amount = paid_amount
	pe.received_amount = received_amount
	pe.allocate_payment_amount = 1
	pe.letter_head = doc.get("letter_head")

	pe.append("references", {
		"reference_doctype": dt,
		"reference_name": dn,
		"bill_no": doc.get("bill_no"),
		"due_date": doc.get("due_date"),
		"total_amount": grand_total,
		"outstanding_amount": outstanding_amount,
		"allocated_amount": outstanding_amount
	})

	pe.setup_party_account_field()
	pe.set_missing_values()
	if party_account and paid_to:
		pe.set_exchange_rate()
		pe.set_amounts()
	return pe

