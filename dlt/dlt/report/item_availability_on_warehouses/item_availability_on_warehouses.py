# Copyright (c) 2013, Tristar Enterprises and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import flt, cint, getdate
from erpnext.stock.report.stock_balance.stock_balance import get_item_reorder_details, get_item_warehouse_map
from erpnext.stock.report.stock_ageing.stock_ageing import get_fifo_queue, get_average_age
from dlt.utils import get_user_permissions_for

def execute(filters=None):
	if not filters: filters = {}

	validate_filters(filters)

	is_sales_user = has_sales_user_role()
	columns = get_columns(filters, is_sales_user)
	item_map = get_item_details(filters)
	iwb_map = get_item_warehouse_map(filters)
	warehouse_list = get_warehouse_list(filters)
	item_ageing = get_fifo_queue(filters)
	data = []
	item_balance = {}
	item_value = {}
	

	for (company, item, warehouse) in sorted(iwb_map):
		row = []
		qty_dict = iwb_map[(company, item, warehouse)]
		item_balance.setdefault((item, item_map[item]["item_group"]), [])
		total_stock_value = 0.00
		for wh in warehouse_list:
			row += [qty_dict.bal_qty] if wh.name in warehouse else [0.00]
			total_stock_value += qty_dict.bal_val if wh.name in warehouse else 0.00

		item_balance[(item, item_map[item]["item_group"])].append(row)
		item_value.setdefault((item, item_map[item]["item_group"]),[])
		item_value[(item, item_map[item]["item_group"])].append(total_stock_value)

	
	# sum bal_qty by item
	for (item, item_group), wh_balance in item_balance.items():
		total_stock_value = sum(item_value[(item, item_group)])
		row = [item, item_group, item_map[item]["barcode"]]
		if not is_sales_user:
			row += [total_stock_value]
		fifo_queue = None
		if item_ageing.get(item):
			fifo_queue = item_ageing[item]["fifo_queue"]

		average_age = 0.00
		if fifo_queue:
			average_age = get_average_age(fifo_queue, filters["to_date"])

		row += [average_age]

		bal_qty = [sum(bal_qty) for bal_qty in zip(*wh_balance)]
		total_qty = sum(bal_qty)
		if len(warehouse_list) > 1:
			row += [total_qty]
		row += bal_qty

		if total_qty > 0:
			data.append(row)
		elif not filters.get("filter_total_zero_qty"):
			data.append(row)
	add_warehouse_column(columns, warehouse_list)
	return columns, data

def get_columns(filters, is_sales_user=False):
	"""return columns"""

	columns = [
		_("Item")+":Link/Item:180",
		_("Item Group")+"::100",
		_("Barcode")+"::100"
	]
	if not is_sales_user:
		columns += [_("Value")+":Currency:100"]

	columns += [_("Age")+":Float:60"]

	return columns

def get_item_details(filters):
	condition = ''
	value = ()
	if filters.get("item_code"):
		condition = "where item_code=%s"
		value = (filters.get("item_code"),)

	items = frappe.db.sql("""
		select name, item_name, stock_uom, item_group, brand, description, barcode
		from tabItem
		{condition}
	""".format(condition=condition), value, as_dict=1)

	item_details = dict((d.name , d) for d in items)

	if filters.get('show_variant_attributes', 0) == 1:
		variant_values = get_variant_values_for(item_details.keys())
		item_details = {k: v.update(variant_values.get(k, {})) for k, v in iteritems(item_details)}

	return item_details


def validate_filters(filters):
	if not (filters.get("item_code") or filters.get("warehouse")):
		sle_count = flt(frappe.db.sql("""select count(name) from `tabStock Ledger Entry`""")[0][0])
		if sle_count > 500000:
			frappe.throw(_("Please set filter based on Item or Warehouse"))
	if not filters.get("company"):
		filters["company"] = frappe.defaults.get_user_default("Company")

def get_warehouse_list(filters):
	condition = ''
	user_permitted_warehouse = get_user_permissions_for("Warehouse")
	value = ()
	if user_permitted_warehouse:
		condition = "and name in %s"
		value = set(user_permitted_warehouse)
	elif not user_permitted_warehouse and filters.get("warehouse"):
		condition = "and name = %s"
		value = filters.get("warehouse")

	return frappe.db.sql("""select name, ifnull(abbr, name) as abbr 
		from `tabWarehouse` where is_group = 0
		{condition}""".format(condition=condition), value, as_dict=1)

def has_sales_user_role():
	user_roles = frappe.get_roles()
	return True if 'Sales User' in user_roles and 'Sales Manager' not in user_roles else False

def add_warehouse_column(columns, warehouse_list):
	if len(warehouse_list) > 1:
		columns += [_("Total")+":Int:40"]

	for wh in warehouse_list:
		columns += [_(wh.abbr)+":Int:54"]

