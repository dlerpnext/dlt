# Copyright (c) 2013, Tristar Enterprises and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns = get_columns()
	data = get_data(filters)
	return columns, data

def get_columns():	
	columns = [
		{"fieldname":"invoice",		"label": _("Sales Invoice"),	"fieldtype": "Link", "options":"Sales Invoice","width": 85},
		{"fieldname":"posting_date","label": _("Date"),	"fieldtype": "Date", "width": 75},
		{"fieldname":"customer",	"label": _("Customer"),			"fieldtype": "Link","options":"Customer","width": 140},
		{"fieldname":"item_code",	"label": _("Item Code"),		"fieldtype":"Link", "options":"Item", "width": 140},
		{"fieldname":"item_name",	"label": _("Item Name"),		"fieldtype": "Data","width": 135},
		{"fieldname":"qty",			"label": _("Invoiced Qty"),		"fieldtype":"FLoat","width": 100},
		{"fieldname":"delivered_qty","label": _("Delivered Qty"),	"fieldtype":"Float","width": 100},
		{"fieldname":"delivery_note","label": _("Delivery Note"),	"fieldtype": "Link", "options":"Delivery Note","width": 100},
		{"fieldname":"warehouse",	"label": _("Warehouse"),		"fieldtype": "Link","options":"Warehouse","width": 140},
		]
	return columns

def get_data(filters):
	condition = ""
	if filters.from_date:
			condition += " AND doc.posting_date >= '%s'"%(filters.from_date)
	if filters.to_date:
			condition += " AND doc.posting_date <= '%s'"%(filters.to_date)
	if filters.company:
		condition +=" AND doc.company = '%s' "%(filters.company)
	if filters.warehouse:
		condition +=" AND doc_item.warehouse = '%s' "%(filters.warehouse)
	if filters.item_code:
		condition +=" AND doc_item.item_code = '%s' "%(filters.item_code)
	query = """SELECT
		doc.name as invoice,
		doc.posting_date,
		doc.customer,
		doc_item.item_code,
		doc_item.item_name,
		doc_item.qty,
		doc_item.delivered_qty,
		doc_item.warehouse,
		doc_dn_item.parent as delivery_note
		FROM `tabSales Invoice` AS doc
		INNER JOIN `tabSales Invoice Item` AS doc_item ON (doc_item.parent = doc.name)
		LEFT JOIN `tabDelivery Note Item` AS doc_dn_item ON (doc_dn_item.against_sales_invoice = doc.name)
		where doc.docstatus = 1
		-- and doc.is_return = 0
		-- and update_stock = 0
		{condition}
		""".format(condition=condition)
	return frappe.db.sql(query, as_dict=1)
