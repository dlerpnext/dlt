// Copyright (c) 2016, Tristar Enterprises LLC and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["PDC"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"reqd": 1,
			"width": "60px"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
			"reqd": 1,
			"width": "60px"
		},
		{
			"fieldname":"enquiry_type",
			"label":__("Enquiry Type"),
			"fieldtype":"Select",
			"default":"PDC",
			"options":"PDC\nCleared\nAll"
		},
		{
			"fieldtype": "Break",
		},
		{
			"fieldname":"payment_type",
			"label":__("Payment Type"),
			"fieldtype":"Select",
			"default":"Receive",
			"options":"Receive\nPay\nInternal Transfer"
		},
		{
			"fieldname":"party_type",
			"label":__("Party Type"),
			"fieldtype":"Link",
			"options":"Party Type"
		},
		{
			"fieldname":"party",
			"label": __("Party"),
			"fieldtype": "Dynamic Link",
			"get_options": function() {
				var party_type = frappe.query_report_filters_by_name.party_type.get_value();
				var party = frappe.query_report_filters_by_name.party.get_value();
				if(party && !party_type) {
					frappe.throw(__("Please select Party Type first"));
				}
				return party_type;
			},
			on_change: function() {
				var party_type = frappe.query_report_filters_by_name.party_type.get_value();
				var party = frappe.query_report_filters_by_name.party.get_value();
				if(!party_type || !party) {
				//	frappe.query_report_filters_by_name.party_name.set_value("");
					return;
				}

				var fieldname = frappe.scrub(party_type) + "_name";
				frappe.db.get_value(party_type, party, fieldname, function(value) {
					//frappe.query_report_filters_by_name.party_name.set_value(value[fieldname]);
				});
				trigger_refresh();
			}
		},
		{
			"fieldname":"company",
			"label": __("Company"),
			"fieldtype": "Link",
			"options": "Company",
			"default": frappe.defaults.get_user_default("Company"),
			"reqd": 1
		},

	]
}
