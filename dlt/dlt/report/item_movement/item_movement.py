# Copyright (c) 2013, Tristar Enterprises and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns = get_columns()
	sl_entries = get_stock_ledger_entries(filters)
	item_details = get_item_details(filters)
	opening_row = get_opening_balance(filters, columns)
	party_details = get_party_details(filters)
	data = []

	if opening_row:
		data.append(opening_row)

	for sle in sl_entries:
		item_detail = item_details[sle.item_code]
		party_detail = party_details[sle.voucher_no]
		row = [p_detail for p_detail in party_detail if p_detail['item_code']== sle.item_code][0]

		data.append([sle.date,
			sle.item_code, 
			row.purpose,
			sle.voucher_no,
			
			row.party,
			(sle.actual_qty if sle.actual_qty > 0 else ''),
			row.rate if sle.actual_qty > 0 else '',
			float(row.rate * sle.actual_qty) if sle.actual_qty > 0 else '',

			(abs(sle.actual_qty) if sle.actual_qty < 0 else ''),
			(abs(row.rate) if sle.actual_qty < 0 else ''),
			(abs(float(row.rate*sle.actual_qty)) if sle.actual_qty < 0 else ''),

			sle.qty_after_transaction,			
			#(sle.incoming_rate if sle.actual_qty > 0 else 0.0),			
			sle.valuation_rate, 
			sle.stock_value,
			sle.warehouse,

			#party_detail.party,
			item_detail.item_name, item_detail.item_group,
			item_detail.brand, item_detail.description,
			item_detail.stock_uom, 

			sle.batch_no, sle.serial_no, sle.company, sle.voucher_type])

	return columns, data

def get_columns():
	columns = [
		_("Date") + ":Datetime:150",
		_("Item") + ":Link/Item:130",
		_("Purpose") + "::110",
		_("Voucher #") + ":Dynamic Link/" + _("Voucher Type") + ":100",
		_("Party") + "::150",
		_("In Qty") + ":Float:55",
		{"label": _("In Rate"), "fieldtype": "Currency", "width": 110, "options": "Company:company:default_currency"},
		{"label": _("In Amount"), "fieldtype": "Currency", "width": 110, "options": "Company:company:default_currency"},
		
		_("Out Qty") + ":Float:85",
		{"label": _("Out Rate"), "fieldtype": "Currency", "width": 110, "options": "Company:company:default_currency"},
		{"label": _("Out Amount"), "fieldtype": "Currency", "width": 110, "options": "Company:company:default_currency"},

		#_("Qty") + ":Float:50",
		_("Balance Qty") + ":Float:100",
		#{"label": _("Incoming Rate"), "fieldtype": "Currency", "width": 110, "options": "Company:company:default_currency"},
		{"label": _("Valuation Rate"), "fieldtype": "Currency", "width": 110,"options": "Company:company:default_currency"},
		{"label": _("Balance Value"), "fieldtype": "Currency", "width": 110, "options": "Company:company:default_currency"},
		_("Warehouse") + ":Link/Warehouse:100",

		_("Item Name") + "::100", _("Item Group") + ":Link/Item Group:100",
		_("Brand") + ":Link/Brand:100", _("Description") + "::200",
		_("Stock UOM") + ":Link/UOM:100",

		_("Batch") + ":Link/Batch:100",
		_("Serial #") + ":Link/Serial No:100",
		{"label": _("Company"), "fieldtype": "Link", "width": 110, "options": "company", "fieldname": "company"},
		_("Voucher Type") + "::110"
	]

	return columns

def get_stock_ledger_entries(filters):
	return frappe.db.sql("""select concat_ws(" ", posting_date, posting_time) as date,
			item_code, warehouse, actual_qty, qty_after_transaction, incoming_rate, valuation_rate,
			stock_value, voucher_type, voucher_no, batch_no, serial_no, company
		from `tabStock Ledger Entry` sle
		where company = %(company)s and
			posting_date between %(from_date)s and %(to_date)s
			{sle_conditions}
			order by posting_date asc, posting_time asc, name asc"""\
		.format(sle_conditions=get_sle_conditions(filters)), filters, as_dict=1)

def get_item_details(filters):
	item_details = {}
	for item in frappe.db.sql("""select name, item_name, description, item_group,
			brand, stock_uom from `tabItem` item {item_conditions}"""\
			.format(item_conditions=get_item_conditions(filters)), filters, as_dict=1):
		item_details.setdefault(item.name, item)

	return item_details

def get_item_conditions(filters):
	conditions = []
	if filters.get("item_code"):
		conditions.append("item.name=%(item_code)s")
	if filters.get("brand"):
		conditions.append("item.brand=%(brand)s")
	if filters.get("item_group"):
		conditions.append(get_item_group_condition(filters.get("item_group")))

	return "where {}".format(" and ".join(conditions)) if conditions else ""

def get_sle_conditions(filters):
	conditions = []
	item_conditions=get_item_conditions(filters)
	if item_conditions:
		conditions.append("""sle.item_code in (select item.name from tabItem item
			{item_conditions})""".format(item_conditions=item_conditions))
	if filters.get("warehouse"):
		warehouse_condition = get_warehouse_condition(filters.get("warehouse"))
		if warehouse_condition:
			conditions.append(warehouse_condition)
	if filters.get("voucher_no"):
		conditions.append("voucher_no=%(voucher_no)s")
	if filters.get("batch_no"):
		conditions.append("batch_no=%(batch_no)s")

	return "and {}".format(" and ".join(conditions)) if conditions else ""

def get_opening_balance(filters, columns):
	if not (filters.item_code and filters.warehouse and filters.from_date):
		return

	from erpnext.stock.stock_ledger import get_previous_sle
	last_entry = get_previous_sle({
		"item_code": filters.item_code,
		"warehouse": get_warehouse_condition(filters.warehouse),
		"posting_date": filters.from_date,
		"posting_time": "00:00:00"
	})

	row = [""]*len(columns)
	row[1] = _("'Opening'")
	for i, v in ((9, 'qty_after_transaction'), (11, 'valuation_rate'), (12, 'stock_value')):
			row[i] = last_entry.get(v, 0)

	return row

def get_warehouse_condition(warehouse):
	warehouse_details = frappe.db.get_value("Warehouse", warehouse, ["lft", "rgt"], as_dict=1)
	if warehouse_details:
		return " exists (select name from `tabWarehouse` wh \
			where wh.lft >= %s and wh.rgt <= %s and sle.warehouse = wh.name)"%(warehouse_details.lft,
			warehouse_details.rgt)

	return ''

def get_item_group_condition(item_group):
	item_group_details = frappe.db.get_value("Item Group", item_group, ["lft", "rgt"], as_dict=1)
	if item_group_details:
		return "item.item_group in (select ig.name from `tabItem Group` ig \
			where ig.lft >= %s and ig.rgt <= %s and item.item_group = ig.name)"%(item_group_details.lft,
			item_group_details.rgt)

	return ''

def get_party_details(filters):
	party_details = {}
	condition = ""
	for doctype in ["Purchase Receipt", "Purchase Invoice","Delivery Note", "Sales Invoice"]:
		if doctype in ["Purchase Receipt", "Purchase Invoice"]:
			party_type = "supplier"
			purpose = "Buying"
		elif doctype in ["Delivery Note","Sales Invoice"]:
			party_type = "customer"
			purpose = "Selling"
		for voucher in frappe.db.sql(""" Select doc.name, 
			doc.{party_type} as party,
			'{party_type}' as party_type,
			'{purpose}' as purpose,
			doc_item.item_code,
			doc_item.item_name,
			sum(doc_item.base_net_amount)/sum(doc_item.qty) as rate 			
			-- doc_item.base_net_rate as rate,
			-- doc_item.base_net_amount as amount
			FROM `tab{doctype}` as doc
			INNER JOIN `tab{doctype} Item` as doc_item ON (doc_item.parent=doc.name)
			where doc.docstatus = 1
			{condition}	
			GROUP BY doc.name, doc.{party_type}, doc_item.item_code
  			""".format(party_type=party_type,doctype=doctype,purpose=purpose, condition=condition), as_dict=1):
			
			party_details.setdefault(voucher.name, []).append(voucher)

	for doctype in ["Stock Entry"]:
		for voucher in frappe.db.sql(""" Select doc.name, 
			'' as party,
			'' as party_type,
			doc.purpose,
			doc_item.item_code,
			doc_item.item_name,
			sum(doc_item.amount)/sum(doc_item.qty) as rate
			-- doc_item.valuation_rate as rate,
			-- doc_item.amount as amount
			FROM `tab{doctype}` as doc
			INNER JOIN `tab{doctype} Detail` as doc_item ON (doc_item.parent=doc.name)
			where doc.docstatus = 1
			{condition}
			GROUP BY doc.name,doc_item.item_code		
  			""".format(party_type=party_type,doctype=doctype,condition=condition), as_dict=1):
			
			party_details.setdefault(voucher.name, []).append(voucher)
	
	return party_details
