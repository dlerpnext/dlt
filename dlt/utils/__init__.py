# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

# util __init__.py

from __future__ import unicode_literals
from frappe.utils import flt, get_datetime, getdate, date_diff, cint, nowdate
from frappe import _, msgprint, throw
import frappe
from frappe.utils.background_jobs import enqueue
from frappe.core.doctype.communication.email import make
from frappe.defaults import get_user_permissions


def validate_warehouse(doc, method):
	default_warehouse = None 
	if frappe.get_meta(doc.doctype).get_field("warehouse") and doc.get("warehouse"):
		default_warehouse = doc.warehouse
	if not default_warehouse:
		default_warehouse = frappe.get_value("Cost Center", doc.cost_center, ["warehouse"])
	if not default_warehouse:
		default_warehouse = frappe.db.get_single_value('Stock Settings','default_warehouse')
	if not default_warehouse:
		return

	if doc.doctype in ["Delivery Note", "Sales Invoice"]:
		for item in doc.get("items"):
			if not item.warehouse:
				item.warehouse = default_warehouse
	else:
		for item in doc.get("items"):
			if not item.warehouse or item.warehouse not in default_warehouse:
				item.warehouse = doc.warehouse


def validate_cost_center(doc, method):
	if frappe.get_meta(doc.doctype).get_field("write_off_cost_center") and doc.write_off_cost_center != doc.cost_center:
		doc.write_off_cost_center = doc.cost_center

	if frappe.get_meta(doc.doctype + ' Item').get_field("cost_center"):
		for item in doc.get("items"):
			if not item.cost_center or item.cost_center not in doc.cost_center:
				item.cost_center = doc.cost_center

	for tax in doc.get("taxes"):
		if not tax.cost_center or tax.cost_center not in doc.cost_center:
			tax.cost_center = doc.cost_center

def send_email_to_printer_with_attachment(doc, method):
	recipients = [frappe.get_value("Cost Center", doc.cost_center, ["printer_address"])]
	if len(recipients) > 0:
		send_email_with_attachment(doc.doctype, doc.name, recipients)

def send_email_with_attachment(doctype, docname, recipients):
	attachments = [frappe.attach_print(doctype=doctype, name=docname, print_letterhead=True)]
	if attachments and recipients:
		frappe.sendmail(recipients = recipients,
				cc = recipients,
				sender="", 
				subject=docname, 
				message="",
				delayed=False,
				attachments=attachments
		)

def get_user_permissions_for(doctype):
	return filter(None, get_user_permissions().get(doctype, []))

