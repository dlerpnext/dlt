# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

# util __init__.py

from __future__ import unicode_literals
from frappe.utils import flt, get_datetime, getdate, date_diff, cint, nowdate
from frappe import _, msgprint, throw
import frappe
import dlt


def validate_net_selling_price(doc, method):

	validate_net_selling_price = False
	if doc.doctype == "Sales Invoice":
		for d in doc.get("items"):
			if not (d.sales_order or d.delivery_note):
				validate_net_selling_price = True
				break

	elif doc.doctype == "Delivery Note":
		for d in doc.get("items"):
			if not (d.against_sales_order or d.against_sales_invoice):
				validate_net_selling_price = True
				break

	elif doc.doctype == "Sales Order":
		for d in doc.get("items"):
			if not d.prevdoc_docname:
				validate_net_selling_price = True
				break


	if not validate_net_selling_price:
		return

	def throw_message(item_name, rate, ref_rate_field):
		if method == "before_submit":
			allow_sales_below_cost_approver = frappe.db.get_single_value('DLT Settings', 'allow_sales_below_cost_approver')
			if not allow_sales_below_cost_approver or allow_sales_below_cost_approver not in frappe.get_roles():
				frappe.throw(_("""Net selling rate for item {0} can not be lower than its {1}.\n Please contact {2}.""")
					.format(item_name, ref_rate_field, allow_sales_below_cost_approver or 'Administrator'))
			elif allow_sales_below_cost_approver not in frappe.get_roles():
				frappe.msgprint(_("""Net selling rate for item {0} is lower than its {1}.""")
					.format(item_name, ref_rate_field))

		else:
			frappe.msgprint(_("""Net selling rate for item {0} is lower than its {1}.""")
				.format(item_name, ref_rate_field))

	for it in doc.get("items"):
		if not it.item_code:
			continue

		last_purchase_rate, is_stock_item = frappe.db.get_value("Item", it.item_code, ["last_purchase_rate", "is_stock_item"])
		last_purchase_rate_in_sales_uom = last_purchase_rate / (it.conversion_factor or 1)

		if flt(it.base_net_rate) < flt(last_purchase_rate_in_sales_uom):
			throw_message(it.item_name, last_purchase_rate_in_sales_uom, "last purchase rate")

		last_valuation_rate = frappe.db.sql("""
			SELECT valuation_rate FROM `tabStock Ledger Entry` WHERE item_code = %s
			AND warehouse = %s AND valuation_rate > 0
			ORDER BY posting_date DESC, posting_time DESC, name DESC LIMIT 1
			""", (it.item_code, it.warehouse))

		if last_valuation_rate:
			last_valuation_rate_in_sales_uom = last_valuation_rate[0][0] / (it.conversion_factor or 1)
			if is_stock_item and flt(it.base_net_rate) < flt(last_valuation_rate_in_sales_uom):
				throw_message(it.item_code, last_valuation_rate_in_sales_uom, "valuation rate")


def validate_trade_license_expiry(doc, method):
	trade_license_expire_on = frappe.db.get_value("Customer", doc.customer, ["trade_license_expire_on"])
	diff = date_diff(trade_license_expire_on, getdate())
	if trade_license_expire_on and (date_diff(getdate(), trade_license_expire_on) > 0):
		frappe.msgprint(_("You cann't create invoice as customer license already expired on {0}".format(trade_license_expire_on)))


def validate_if_delivery_note_already_exists(doc, method):
	if not doc.get("__islocal"):
		return

	against_sales_invoice = [d.against_sales_invoice for d in doc.get("items") if d.parent==doc.name]
	dn = frappe.db.sql(""" select parent from `tabDelivery Note Item` 
				where docstatus <> 2 
				and against_sales_invoice = %s 
				and parent <> %s limit 1""", (against_sales_invoice[0], doc.name))
	if dn:
		frappe.msgprint(_("Delivery Note {0} already created for this sales invoice".format(dn[0][0])))

def send_email_to_printer_with_attachment(doc, method):
	send_email_to_printer = None
	if doc.doctype == "Delivery Note":
		send_email_to_printer = frappe.db.get_single_value('DLT Settings', 'send_email_to_printer_on_delivery_note_submit')
	elif doc.doctype == "Sales Invoice":
		send_email_to_printer = frappe.db.get_single_value('DLT Settings', 'send_email_to_printer_on_sales_invoice_submit')

	if send_email_to_printer:
		dlt.utils.send_email_to_printer_with_attachment(doc, method)



