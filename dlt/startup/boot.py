# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt"


from __future__ import unicode_literals
import frappe

def boot_session(bootinfo):
	"""boot session - send website info if guest"""
	if frappe.session['user']!='Guest':
		load_cost_center(bootinfo)

def load_cost_center(bootinfo):
	bootinfo.docs += frappe.db.sql("""select name, warehouse, cash_account, printer_address from `tabCost Center` where is_group=0""",
			as_dict=1, update={"doctype":":Cost Center"})


